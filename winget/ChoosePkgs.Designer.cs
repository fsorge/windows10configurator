﻿namespace Windows10Configurator.winget
{
    partial class ChoosePkgs
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ChoosePkgs));
            this.label1 = new System.Windows.Forms.Label();
            this.wingetSelectedFileLbl = new System.Windows.Forms.Label();
            this.appsListBox = new System.Windows.Forms.CheckedListBox();
            this.installBtn = new System.Windows.Forms.Button();
            this.cancelBtn = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.selectAll = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.selectNone = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.invertSelection = new System.Windows.Forms.ToolStripButton();
            this.label2 = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            this.toolStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Segoe UI Light", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(25, 22);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(169, 45);
            this.label1.TabIndex = 1;
            this.label1.Text = "Install apps";
            // 
            // wingetSelectedFileLbl
            // 
            this.wingetSelectedFileLbl.AutoSize = true;
            this.wingetSelectedFileLbl.Location = new System.Drawing.Point(195, 42);
            this.wingetSelectedFileLbl.Name = "wingetSelectedFileLbl";
            this.wingetSelectedFileLbl.Size = new System.Drawing.Size(104, 20);
            this.wingetSelectedFileLbl.TabIndex = 9;
            this.wingetSelectedFileLbl.Text = "Selected file: ";
            this.wingetSelectedFileLbl.Visible = false;
            // 
            // appsListBox
            // 
            this.appsListBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.appsListBox.CheckOnClick = true;
            this.appsListBox.FormattingEnabled = true;
            this.appsListBox.Location = new System.Drawing.Point(0, 37);
            this.appsListBox.Name = "appsListBox";
            this.appsListBox.Size = new System.Drawing.Size(974, 464);
            this.appsListBox.TabIndex = 10;
            this.appsListBox.ItemCheck += new System.Windows.Forms.ItemCheckEventHandler(this.appsListBox_ItemCheck);
            // 
            // installBtn
            // 
            this.installBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.installBtn.Enabled = false;
            this.installBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.installBtn.Location = new System.Drawing.Point(856, 599);
            this.installBtn.Name = "installBtn";
            this.installBtn.Size = new System.Drawing.Size(151, 51);
            this.installBtn.TabIndex = 11;
            this.installBtn.Text = "INSTALL";
            this.installBtn.UseVisualStyleBackColor = true;
            this.installBtn.Click += new System.EventHandler(this.installBtn_Click);
            // 
            // cancelBtn
            // 
            this.cancelBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.cancelBtn.Location = new System.Drawing.Point(33, 606);
            this.cancelBtn.Name = "cancelBtn";
            this.cancelBtn.Size = new System.Drawing.Size(128, 36);
            this.cancelBtn.TabIndex = 12;
            this.cancelBtn.Text = "Cancel";
            this.cancelBtn.UseVisualStyleBackColor = true;
            this.cancelBtn.Click += new System.EventHandler(this.cancelBtn_Click);
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel1.Controls.Add(this.toolStrip1);
            this.panel1.Controls.Add(this.appsListBox);
            this.panel1.Location = new System.Drawing.Point(33, 82);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(974, 501);
            this.panel1.TabIndex = 13;
            // 
            // toolStrip1
            // 
            this.toolStrip1.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolStrip1.ImageScalingSize = new System.Drawing.Size(24, 24);
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.selectAll,
            this.toolStripSeparator1,
            this.selectNone,
            this.toolStripSeparator2,
            this.invertSelection});
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(974, 38);
            this.toolStrip1.TabIndex = 11;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // selectAll
            // 
            this.selectAll.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.selectAll.Image = ((System.Drawing.Image)(resources.GetObject("selectAll.Image")));
            this.selectAll.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.selectAll.Name = "selectAll";
            this.selectAll.Size = new System.Drawing.Size(84, 33);
            this.selectAll.Text = "Select all";
            this.selectAll.Click += new System.EventHandler(this.selectAll_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 38);
            // 
            // selectNone
            // 
            this.selectNone.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.selectNone.Image = ((System.Drawing.Image)(resources.GetObject("selectNone.Image")));
            this.selectNone.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.selectNone.Name = "selectNone";
            this.selectNone.Size = new System.Drawing.Size(107, 33);
            this.selectNone.Text = "Select none";
            this.selectNone.Click += new System.EventHandler(this.selectNone_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(6, 38);
            // 
            // invertSelection
            // 
            this.invertSelection.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.invertSelection.Image = ((System.Drawing.Image)(resources.GetObject("invertSelection.Image")));
            this.invertSelection.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.invertSelection.Name = "invertSelection";
            this.invertSelection.Size = new System.Drawing.Size(135, 33);
            this.invertSelection.Text = "Invert selection";
            this.invertSelection.Click += new System.EventHandler(this.invertSelection_Click);
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(670, 604);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(180, 40);
            this.label2.TabIndex = 14;
            this.label2.Text = "Estimate download size:\r\nEstimating...";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.label2.Visible = false;
            // 
            // ChoosePkgs
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1037, 672);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.cancelBtn);
            this.Controls.Add(this.installBtn);
            this.Controls.Add(this.wingetSelectedFileLbl);
            this.Controls.Add(this.label1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(1059, 728);
            this.Name = "ChoosePkgs";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Installing applications";
            this.Load += new System.EventHandler(this.ChoosePkgs_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label wingetSelectedFileLbl;
        private System.Windows.Forms.CheckedListBox appsListBox;
        private System.Windows.Forms.Button installBtn;
        private System.Windows.Forms.Button cancelBtn;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripButton selectAll;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripButton selectNone;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripButton invertSelection;
        private System.Windows.Forms.Label label2;
    }
}