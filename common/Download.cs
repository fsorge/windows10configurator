﻿using System;
using System.ComponentModel;
using System.IO;
using System.Net;

namespace Windows10Configurator.common
{
    class Download
    {
        public static void Go(string savePath, string fileName, string downloadURL, DownloadProgressChangedEventHandler pChanged, AsyncCompletedEventHandler downloadCompleted)
        {
            Directory.CreateDirectory(savePath);

            using (WebClient wc = new WebClient())
            {
                wc.DownloadProgressChanged += pChanged;

                wc.DownloadFileCompleted += downloadCompleted;

                wc.DownloadFileAsync(new Uri(downloadURL), Path.Combine(savePath, fileName));
            }
        }
    }
}
