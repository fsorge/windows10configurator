﻿using System.IO;

namespace Windows10Configurator.config
{
    public class App
    {
        private App app;

        public const string WinGetDownloadURL = "https://github.com/microsoft/winget-cli/releases/download/v0.1.41821-preview/Microsoft.DesktopAppInstaller_8wekyb3d8bbwe.appxbundle";

        public const string Windows10DebloaterDownloadURL = "https://raw.githubusercontent.com/Sycnex/Windows10Debloater/master/Windows10Debloater.ps1";

        public const float Version = 1.0f;
        public static string DefaultDownloadPath => Path.Combine(Path.GetTempPath(), "windows10configurator");

        private App() { }

        public App GetInstance()
        {
            if (app == null)
            {
                app = new App();
            }

            return app;
        }
    }
}
