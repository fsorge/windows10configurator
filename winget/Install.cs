﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Windows.Forms;

namespace Windows10Configurator.winget
{
    public partial class Install : Form
    {

        private readonly string[] packages;
        private readonly Queue<string> packagesLeft = new Queue<string>();

        private readonly List<string> errors = new List<string>();

        public Install(string[] packages)
        {
            InitializeComponent();

            this.packages = packages;
        }

        private void Install_Load(object sender, EventArgs e)
        {
            detailedListView.AutoResizeColumns(ColumnHeaderAutoResizeStyle.ColumnContent);
            detailedListView.AutoResizeColumns(ColumnHeaderAutoResizeStyle.HeaderSize);

            foreach (string package in packages)
            {
                string[] row = {  DateTime.Now.ToString("yyyy-MM-dd HH:mm"), package, "Waiting..." };
                detailedListView.Items.Add(new ListViewItem(row));
                packagesLeft.Enqueue(package);
            }

            foreach (ColumnHeader column in detailedListView.Columns)
            {
                column.Width = -2;
            }

            updateProcessedLbl();
        }

        private void updateProcessedLbl()
        {
            this.processedLbl.Text = $"Processed: {packages.Length - packagesLeft.Count} / {packages.Length}";
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            timer1.Stop();

            statusHeaderLbl.Text = "Processing...";

            int i = 0;
            while (packagesLeft.Count > 0)
            {
                using (Process process = new Process())
                {
                    string package = packagesLeft.Dequeue();

                    List<string> row = new List<string>();
                    row.Add(DateTime.Now.ToString("yyyy-MM-dd HH:mm"));
                    row.Add(package);
                    row.Add("Downloading & installing...");
                    detailedListView.Items[i] = new ListViewItem(row.ToArray());

                    process.StartInfo.FileName = $"winget";
                    process.StartInfo.Arguments = $"install -e {package}";
                    process.EnableRaisingEvents = true;
                    process.Start();
                    process.WaitForExit();

                    row = new List<string>();
                    row.Add(DateTime.Now.ToString("yyyy-MM-dd HH:mm"));
                    row.Add(package);

                    switch (process.ExitCode)
                    {
                        case 0:
                            row.Add("Installed correctly");
                            break;
                        default:
                            this.errors.Add(package);
                            row.Add("An error occurred");
                            break;
                    }

                    detailedListView.Items[i] = new ListViewItem(row.ToArray());
                }
                i++;
                updateProcessedLbl();
            }

            this.Text = "Finished!";
            statusHeaderLbl.Text = "Finished!";
            this.progressBar1.Style = ProgressBarStyle.Blocks;

            if (this.errors.Count == 0)
            {
                MessageBox.Show("Applications installed succesfully!", "Success!", MessageBoxButtons.OK, MessageBoxIcon.Information);
                this.Close();
            }
            else if (this.errors.Count == this.packages.Length)
            {
                DialogResult r = MessageBox.Show("All installations failed. Would you like to retry?", "Error", MessageBoxButtons.RetryCancel, MessageBoxIcon.Error);
                if (r == DialogResult.Retry)
                {
                    Install install = new Install(this.packages);
                    install.ShowDialog();
                    this.Close();
                }
            }
            else
            {
                List<string> examples = new List<string>();
                for (int j = 0; j < this.errors.Count && j < 5; j++)
                {
                    examples.Add(this.errors[j]);
                }
                if (this.errors.Count > 5)
                {
                    examples.Add($"and {this.errors.Count - 5} more");
                }

                DialogResult r = MessageBox.Show($"Some apps failed to install ({string.Join(", ", examples)}). Would you like to retry?", "Error", MessageBoxButtons.RetryCancel, MessageBoxIcon.Error);
                if (r == DialogResult.Retry)
                {
                    Install install = new Install(this.errors.ToArray());
                    install.ShowDialog();
                    this.Close();
                }
            }
        }
    }
}
