﻿namespace Windows10Configurator
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.wingetSelectedFileLbl = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.wingetDownloadBtn = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.wingetInstallBtn = new System.Windows.Forms.Button();
            this.wingetChooseFileBtn = new System.Windows.Forms.Button();
            this.label6 = new System.Windows.Forms.Label();
            this.wingetDlProgressBar = new System.Windows.Forms.ProgressBar();
            this.wingetInstallStatusLbl = new System.Windows.Forms.Label();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.disableBloatwareListLink = new System.Windows.Forms.LinkLabel();
            this.disableBloatwareExLbl = new System.Windows.Forms.Label();
            this.disableBloatwareCreditLink = new System.Windows.Forms.LinkLabel();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.disableBloatwareExBtn = new System.Windows.Forms.Button();
            this.disableBloatwareInstallStatusLbl = new System.Windows.Forms.Label();
            this.disableBloatwareDlProgressBar = new System.Windows.Forms.ProgressBar();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.disableBloatwareDlBtn = new System.Windows.Forms.Button();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label14 = new System.Windows.Forms.Label();
            this.miscDisableStartupItemsBtn = new System.Windows.Forms.Button();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.tabPage5 = new System.Windows.Forms.TabPage();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.aboutContributeProjectBtn = new System.Windows.Forms.Button();
            this.aboutVisitAuthorWebsiteBtn = new System.Windows.Forms.Button();
            this.label16 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.aboutVersionLbl = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.restartWithElevatedPrivBtn = new System.Windows.Forms.Button();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.elevatedPanel = new System.Windows.Forms.Panel();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage3.SuspendLayout();
            this.tabPage4.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.tabPage5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.elevatedPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Controls.Add(this.tabPage4);
            this.tabControl1.Controls.Add(this.tabPage5);
            this.tabControl1.Font = new System.Drawing.Font("Segoe UI Light", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tabControl1.Location = new System.Drawing.Point(0, 64);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(1277, 547);
            this.tabControl1.TabIndex = 3;
            // 
            // tabPage1
            // 
            this.tabPage1.AutoScroll = true;
            this.tabPage1.Controls.Add(this.wingetSelectedFileLbl);
            this.tabPage1.Controls.Add(this.label3);
            this.tabPage1.Controls.Add(this.label1);
            this.tabPage1.Controls.Add(this.label2);
            this.tabPage1.Controls.Add(this.label4);
            this.tabPage1.Controls.Add(this.wingetInstallBtn);
            this.tabPage1.Controls.Add(this.wingetChooseFileBtn);
            this.tabPage1.Controls.Add(this.label6);
            this.tabPage1.Controls.Add(this.wingetDlProgressBar);
            this.tabPage1.Controls.Add(this.wingetInstallStatusLbl);
            this.tabPage1.Controls.Add(this.wingetDownloadBtn);
            this.tabPage1.Font = new System.Drawing.Font("Segoe UI Light", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tabPage1.Location = new System.Drawing.Point(4, 41);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(1269, 502);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Install apps";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // wingetSelectedFileLbl
            // 
            this.wingetSelectedFileLbl.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.wingetSelectedFileLbl.BackColor = System.Drawing.Color.White;
            this.wingetSelectedFileLbl.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.wingetSelectedFileLbl.Enabled = false;
            this.wingetSelectedFileLbl.Location = new System.Drawing.Point(377, 308);
            this.wingetSelectedFileLbl.Multiline = true;
            this.wingetSelectedFileLbl.Name = "wingetSelectedFileLbl";
            this.wingetSelectedFileLbl.Size = new System.Drawing.Size(854, 33);
            this.wingetSelectedFileLbl.TabIndex = 13;
            this.wingetSelectedFileLbl.Text = "Selected file:\r\n";
            this.wingetSelectedFileLbl.Visible = false;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(33, 133);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(159, 30);
            this.label3.TabIndex = 2;
            this.label3.Text = "1. Install Winget";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Segoe UI Light", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(23, 27);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(169, 45);
            this.label1.TabIndex = 0;
            this.label1.Text = "Install apps";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(26, 72);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(348, 30);
            this.label2.TabIndex = 1;
            this.label2.Text = "Automatically install apps from a list";
            // 
            // wingetDownloadBtn
            // 
            this.wingetDownloadBtn.Location = new System.Drawing.Point(38, 167);
            this.wingetDownloadBtn.Name = "wingetDownloadBtn";
            this.wingetDownloadBtn.Size = new System.Drawing.Size(328, 57);
            this.wingetDownloadBtn.TabIndex = 3;
            this.wingetDownloadBtn.Text = "Download && install";
            this.wingetDownloadBtn.UseVisualStyleBackColor = true;
            this.wingetDownloadBtn.Click += new System.EventHandler(this.button1_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(33, 262);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(138, 30);
            this.label4.TabIndex = 6;
            this.label4.Text = "2. Choose file";
            // 
            // wingetInstallBtn
            // 
            this.wingetInstallBtn.Enabled = false;
            this.wingetInstallBtn.Location = new System.Drawing.Point(39, 421);
            this.wingetInstallBtn.Name = "wingetInstallBtn";
            this.wingetInstallBtn.Size = new System.Drawing.Size(328, 57);
            this.wingetInstallBtn.TabIndex = 12;
            this.wingetInstallBtn.Text = "Install";
            this.wingetInstallBtn.UseVisualStyleBackColor = true;
            this.wingetInstallBtn.Click += new System.EventHandler(this.wingetInstallBtn_Click);
            // 
            // wingetChooseFileBtn
            // 
            this.wingetChooseFileBtn.Enabled = false;
            this.wingetChooseFileBtn.Location = new System.Drawing.Point(39, 295);
            this.wingetChooseFileBtn.Name = "wingetChooseFileBtn";
            this.wingetChooseFileBtn.Size = new System.Drawing.Size(328, 57);
            this.wingetChooseFileBtn.TabIndex = 7;
            this.wingetChooseFileBtn.Text = "Choose";
            this.wingetChooseFileBtn.UseVisualStyleBackColor = true;
            this.wingetChooseFileBtn.Click += new System.EventHandler(this.wingetChooseFileBtn_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(33, 388);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(89, 30);
            this.label6.TabIndex = 11;
            this.label6.Text = "3. Install";
            // 
            // wingetDlProgressBar
            // 
            this.wingetDlProgressBar.Location = new System.Drawing.Point(39, 212);
            this.wingetDlProgressBar.Name = "wingetDlProgressBar";
            this.wingetDlProgressBar.Size = new System.Drawing.Size(326, 11);
            this.wingetDlProgressBar.TabIndex = 5;
            this.wingetDlProgressBar.Visible = false;
            // 
            // wingetInstallStatusLbl
            // 
            this.wingetInstallStatusLbl.AutoSize = true;
            this.wingetInstallStatusLbl.Location = new System.Drawing.Point(372, 180);
            this.wingetInstallStatusLbl.Name = "wingetInstallStatusLbl";
            this.wingetInstallStatusLbl.Size = new System.Drawing.Size(380, 30);
            this.wingetInstallStatusLbl.TabIndex = 4;
            this.wingetInstallStatusLbl.Text = "Checking if Winget is already installed...";
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.disableBloatwareListLink);
            this.tabPage3.Controls.Add(this.disableBloatwareExLbl);
            this.tabPage3.Controls.Add(this.disableBloatwareCreditLink);
            this.tabPage3.Controls.Add(this.label11);
            this.tabPage3.Controls.Add(this.label10);
            this.tabPage3.Controls.Add(this.disableBloatwareExBtn);
            this.tabPage3.Controls.Add(this.disableBloatwareInstallStatusLbl);
            this.tabPage3.Controls.Add(this.disableBloatwareDlProgressBar);
            this.tabPage3.Controls.Add(this.label7);
            this.tabPage3.Controls.Add(this.label8);
            this.tabPage3.Controls.Add(this.label9);
            this.tabPage3.Controls.Add(this.disableBloatwareDlBtn);
            this.tabPage3.Font = new System.Drawing.Font("Segoe UI Light", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tabPage3.Location = new System.Drawing.Point(4, 41);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage3.Size = new System.Drawing.Size(1269, 502);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "Remove and disable bloatware";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // disableBloatwareListLink
            // 
            this.disableBloatwareListLink.AutoSize = true;
            this.disableBloatwareListLink.Location = new System.Drawing.Point(266, 102);
            this.disableBloatwareListLink.Name = "disableBloatwareListLink";
            this.disableBloatwareListLink.Size = new System.Drawing.Size(78, 30);
            this.disableBloatwareListLink.TabIndex = 15;
            this.disableBloatwareListLink.TabStop = true;
            this.disableBloatwareListLink.Text = "this list";
            this.disableBloatwareListLink.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.disableBloatwareListLink_LinkClicked);
            // 
            // disableBloatwareExLbl
            // 
            this.disableBloatwareExLbl.AutoSize = true;
            this.disableBloatwareExLbl.Location = new System.Drawing.Point(372, 313);
            this.disableBloatwareExLbl.Name = "disableBloatwareExLbl";
            this.disableBloatwareExLbl.Size = new System.Drawing.Size(344, 30);
            this.disableBloatwareExLbl.TabIndex = 14;
            this.disableBloatwareExLbl.Text = "Started. Waiting for script to finish...";
            this.disableBloatwareExLbl.Visible = false;
            // 
            // disableBloatwareCreditLink
            // 
            this.disableBloatwareCreditLink.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.disableBloatwareCreditLink.AutoSize = true;
            this.disableBloatwareCreditLink.Location = new System.Drawing.Point(792, 468);
            this.disableBloatwareCreditLink.Name = "disableBloatwareCreditLink";
            this.disableBloatwareCreditLink.Size = new System.Drawing.Size(159, 30);
            this.disableBloatwareCreditLink.TabIndex = 13;
            this.disableBloatwareCreditLink.TabStop = true;
            this.disableBloatwareCreditLink.Text = "View on GitHub";
            this.disableBloatwareCreditLink.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.disableBloatwareCreditLink_LinkClicked);
            // 
            // label11
            // 
            this.label11.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(33, 468);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(753, 30);
            this.label11.TabIndex = 12;
            this.label11.Text = "Windows10Debloater is brought to you by Sycnex and the awesome community.";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(33, 266);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(106, 30);
            this.label10.TabIndex = 10;
            this.label10.Text = "2. Execute";
            // 
            // disableBloatwareExBtn
            // 
            this.disableBloatwareExBtn.Enabled = false;
            this.disableBloatwareExBtn.Location = new System.Drawing.Point(38, 300);
            this.disableBloatwareExBtn.Name = "disableBloatwareExBtn";
            this.disableBloatwareExBtn.Size = new System.Drawing.Size(328, 57);
            this.disableBloatwareExBtn.TabIndex = 11;
            this.disableBloatwareExBtn.Text = "Run Windows10Debloater";
            this.disableBloatwareExBtn.UseVisualStyleBackColor = true;
            this.disableBloatwareExBtn.Click += new System.EventHandler(this.disableBloatwareExBtn_Click);
            // 
            // disableBloatwareInstallStatusLbl
            // 
            this.disableBloatwareInstallStatusLbl.AutoSize = true;
            this.disableBloatwareInstallStatusLbl.Location = new System.Drawing.Point(372, 199);
            this.disableBloatwareInstallStatusLbl.Name = "disableBloatwareInstallStatusLbl";
            this.disableBloatwareInstallStatusLbl.Size = new System.Drawing.Size(151, 30);
            this.disableBloatwareInstallStatusLbl.TabIndex = 9;
            this.disableBloatwareInstallStatusLbl.Text = "Downloading...";
            this.disableBloatwareInstallStatusLbl.Visible = false;
            // 
            // disableBloatwareDlProgressBar
            // 
            this.disableBloatwareDlProgressBar.Location = new System.Drawing.Point(39, 232);
            this.disableBloatwareDlProgressBar.Name = "disableBloatwareDlProgressBar";
            this.disableBloatwareDlProgressBar.Size = new System.Drawing.Size(326, 11);
            this.disableBloatwareDlProgressBar.TabIndex = 8;
            this.disableBloatwareDlProgressBar.Visible = false;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(33, 152);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(327, 30);
            this.label7.TabIndex = 6;
            this.label7.Text = "1. Download Windows10Debloater";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(26, 72);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(1227, 60);
            this.label8.TabIndex = 5;
            this.label8.Text = "Windows 10 is full of bloatware that slows down your PC and can send info to Micr" +
    "osoft. This will also remove useless modern apps.\r\nFor a complete list, check   " +
    "          .";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Segoe UI Light", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(23, 27);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(434, 45);
            this.label9.TabIndex = 4;
            this.label9.Text = "Remove and disable bloatware";
            // 
            // disableBloatwareDlBtn
            // 
            this.disableBloatwareDlBtn.Location = new System.Drawing.Point(38, 186);
            this.disableBloatwareDlBtn.Name = "disableBloatwareDlBtn";
            this.disableBloatwareDlBtn.Size = new System.Drawing.Size(328, 57);
            this.disableBloatwareDlBtn.TabIndex = 7;
            this.disableBloatwareDlBtn.Text = "Download";
            this.disableBloatwareDlBtn.UseVisualStyleBackColor = true;
            this.disableBloatwareDlBtn.Click += new System.EventHandler(this.disableBloatwareDlBtn_Click);
            // 
            // tabPage4
            // 
            this.tabPage4.Controls.Add(this.groupBox1);
            this.tabPage4.Controls.Add(this.label12);
            this.tabPage4.Controls.Add(this.label13);
            this.tabPage4.Font = new System.Drawing.Font("Segoe UI Light", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tabPage4.Location = new System.Drawing.Point(4, 41);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage4.Size = new System.Drawing.Size(1269, 502);
            this.tabPage4.TabIndex = 3;
            this.tabPage4.Text = "Misc";
            this.tabPage4.UseVisualStyleBackColor = true;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label14);
            this.groupBox1.Controls.Add(this.miscDisableStartupItemsBtn);
            this.groupBox1.Location = new System.Drawing.Point(31, 124);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(403, 196);
            this.groupBox1.TabIndex = 8;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Disable startup apps";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Segoe UI Light", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(19, 33);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(360, 84);
            this.label14.TabIndex = 8;
            this.label14.Text = "Disable each app that doesn\'t fit in one of\r\nthese categories: antivirus, cloud, " +
    "desired-\r\napps.";
            // 
            // miscDisableStartupItemsBtn
            // 
            this.miscDisableStartupItemsBtn.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.miscDisableStartupItemsBtn.Location = new System.Drawing.Point(18, 125);
            this.miscDisableStartupItemsBtn.Name = "miscDisableStartupItemsBtn";
            this.miscDisableStartupItemsBtn.Size = new System.Drawing.Size(363, 55);
            this.miscDisableStartupItemsBtn.TabIndex = 0;
            this.miscDisableStartupItemsBtn.Text = "Disable startup apps";
            this.miscDisableStartupItemsBtn.UseVisualStyleBackColor = true;
            this.miscDisableStartupItemsBtn.Click += new System.EventHandler(this.miscDisableStartupItemsBtn_Click);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(26, 72);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(726, 30);
            this.label12.TabIndex = 7;
            this.label12.Text = "Something more you can do to make your Windows 10 faster and truly yours.";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Segoe UI Light", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(23, 27);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(191, 45);
            this.label13.TabIndex = 6;
            this.label13.Text = "Misc settings";
            // 
            // tabPage5
            // 
            this.tabPage5.Controls.Add(this.pictureBox1);
            this.tabPage5.Controls.Add(this.aboutContributeProjectBtn);
            this.tabPage5.Controls.Add(this.aboutVisitAuthorWebsiteBtn);
            this.tabPage5.Controls.Add(this.label16);
            this.tabPage5.Controls.Add(this.label18);
            this.tabPage5.Controls.Add(this.label15);
            this.tabPage5.Controls.Add(this.aboutVersionLbl);
            this.tabPage5.Controls.Add(this.label17);
            this.tabPage5.Font = new System.Drawing.Font("Segoe UI Light", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tabPage5.Location = new System.Drawing.Point(4, 41);
            this.tabPage5.Name = "tabPage5";
            this.tabPage5.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage5.Size = new System.Drawing.Size(1269, 502);
            this.tabPage5.TabIndex = 5;
            this.tabPage5.Text = "About";
            this.tabPage5.UseVisualStyleBackColor = true;
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox1.BackgroundImage")));
            this.pictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pictureBox1.Location = new System.Drawing.Point(60, 95);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(224, 206);
            this.pictureBox1.TabIndex = 13;
            this.pictureBox1.TabStop = false;
            // 
            // aboutContributeProjectBtn
            // 
            this.aboutContributeProjectBtn.Location = new System.Drawing.Point(647, 234);
            this.aboutContributeProjectBtn.Name = "aboutContributeProjectBtn";
            this.aboutContributeProjectBtn.Size = new System.Drawing.Size(293, 50);
            this.aboutContributeProjectBtn.TabIndex = 12;
            this.aboutContributeProjectBtn.Text = "Contribute to project";
            this.aboutContributeProjectBtn.UseVisualStyleBackColor = true;
            this.aboutContributeProjectBtn.Click += new System.EventHandler(this.aboutContributeProjectBtn_Click);
            // 
            // aboutVisitAuthorWebsiteBtn
            // 
            this.aboutVisitAuthorWebsiteBtn.Location = new System.Drawing.Point(320, 234);
            this.aboutVisitAuthorWebsiteBtn.Name = "aboutVisitAuthorWebsiteBtn";
            this.aboutVisitAuthorWebsiteBtn.Size = new System.Drawing.Size(293, 50);
            this.aboutVisitAuthorWebsiteBtn.TabIndex = 11;
            this.aboutVisitAuthorWebsiteBtn.Text = "Visit author\'s website";
            this.aboutVisitAuthorWebsiteBtn.UseVisualStyleBackColor = true;
            this.aboutVisitAuthorWebsiteBtn.Click += new System.EventHandler(this.aboutVisitAuthorWebsiteBtn_Click);
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(317, 164);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(83, 30);
            this.label16.TabIndex = 10;
            this.label16.Text = "Author:";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(401, 164);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(164, 30);
            this.label18.TabIndex = 9;
            this.label18.Text = "Francesco Sorge";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(315, 120);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(85, 30);
            this.label15.TabIndex = 8;
            this.label15.Text = "Version:";
            // 
            // aboutVersionLbl
            // 
            this.aboutVersionLbl.AutoSize = true;
            this.aboutVersionLbl.Location = new System.Drawing.Point(401, 120);
            this.aboutVersionLbl.Name = "aboutVersionLbl";
            this.aboutVersionLbl.Size = new System.Drawing.Size(40, 30);
            this.aboutVersionLbl.TabIndex = 7;
            this.aboutVersionLbl.Text = "0.0";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Segoe UI Light", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(23, 27);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(434, 45);
            this.label17.TabIndex = 6;
            this.label17.Text = "About Windows10Configurator";
            // 
            // restartWithElevatedPrivBtn
            // 
            this.restartWithElevatedPrivBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.restartWithElevatedPrivBtn.Font = new System.Drawing.Font("Segoe UI Light", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.restartWithElevatedPrivBtn.Location = new System.Drawing.Point(795, 8);
            this.restartWithElevatedPrivBtn.Name = "restartWithElevatedPrivBtn";
            this.restartWithElevatedPrivBtn.Size = new System.Drawing.Size(474, 42);
            this.restartWithElevatedPrivBtn.TabIndex = 13;
            this.restartWithElevatedPrivBtn.Text = "Restart with elevated privileges";
            this.restartWithElevatedPrivBtn.UseVisualStyleBackColor = true;
            this.restartWithElevatedPrivBtn.Click += new System.EventHandler(this.restartWithElevatedPrivBtn_Click);
            // 
            // textBox1
            // 
            this.textBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBox1.BackColor = System.Drawing.Color.Orange;
            this.textBox1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox1.Font = new System.Drawing.Font("Segoe UI Light", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox1.ForeColor = System.Drawing.Color.White;
            this.textBox1.Location = new System.Drawing.Point(160, 13);
            this.textBox1.Multiline = true;
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(625, 30);
            this.textBox1.TabIndex = 12;
            this.textBox1.Text = "Windows10Configurator works better with elevated privileges.";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Segoe UI Light", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.White;
            this.label5.Location = new System.Drawing.Point(12, 5);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(142, 45);
            this.label5.TabIndex = 11;
            this.label5.Text = "Warning!";
            // 
            // elevatedPanel
            // 
            this.elevatedPanel.BackColor = System.Drawing.Color.Orange;
            this.elevatedPanel.Controls.Add(this.restartWithElevatedPrivBtn);
            this.elevatedPanel.Controls.Add(this.label5);
            this.elevatedPanel.Controls.Add(this.textBox1);
            this.elevatedPanel.Dock = System.Windows.Forms.DockStyle.Top;
            this.elevatedPanel.Location = new System.Drawing.Point(0, 0);
            this.elevatedPanel.Name = "elevatedPanel";
            this.elevatedPanel.Size = new System.Drawing.Size(1277, 58);
            this.elevatedPanel.TabIndex = 4;
            this.elevatedPanel.Visible = false;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1277, 610);
            this.Controls.Add(this.elevatedPanel);
            this.Controls.Add(this.tabControl1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MinimumSize = new System.Drawing.Size(1299, 666);
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Windows10Configurator";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.tabPage3.ResumeLayout(false);
            this.tabPage3.PerformLayout();
            this.tabPage4.ResumeLayout(false);
            this.tabPage4.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.tabPage5.ResumeLayout(false);
            this.tabPage5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.elevatedPanel.ResumeLayout(false);
            this.elevatedPanel.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label wingetInstallStatusLbl;
        private System.Windows.Forms.Button wingetDownloadBtn;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ProgressBar wingetDlProgressBar;
        private System.Windows.Forms.Button wingetChooseFileBtn;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button restartWithElevatedPrivBtn;
        private System.Windows.Forms.Button wingetInstallBtn;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox wingetSelectedFileLbl;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Button disableBloatwareDlBtn;
        private System.Windows.Forms.ProgressBar disableBloatwareDlProgressBar;
        private System.Windows.Forms.Label disableBloatwareInstallStatusLbl;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Button disableBloatwareExBtn;
        private System.Windows.Forms.LinkLabel disableBloatwareCreditLink;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label disableBloatwareExLbl;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Button miscDisableStartupItemsBtn;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.LinkLabel disableBloatwareListLink;
        private System.Windows.Forms.Panel elevatedPanel;
        private System.Windows.Forms.TabPage tabPage5;
        private System.Windows.Forms.Label aboutVersionLbl;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Button aboutContributeProjectBtn;
        private System.Windows.Forms.Button aboutVisitAuthorWebsiteBtn;
        private System.Windows.Forms.PictureBox pictureBox1;
    }
}

